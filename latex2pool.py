#!/usr/local/bin/python3
import sys
import re
import pathlib
import hashlib
import tempfile
import shutil
import os
import subprocess
import json
import BlackboardQuiz


if len(sys.argv) != 4:
	print("Please provide three arguments:")
	print("Input LaTeX file in ./input (with Question and Answer environments in it)")
	print("Desired Blackboard package name")
	print("Desired Blackboard pool name")
	print("Output will go in ./output")
	sys.exit()

texfile = pathlib.Path(sys.argv[1]).absolute()
packagename = sys.argv[2]
poolname = sys.argv[3]

cwd = pathlib.Path().absolute()
workdir = cwd / "workdir"
imagedir = workdir / "img"

if not texfile.parts[:-1] == workdir.parts:
	print("Warning: script may assume the input is in ./workdir and it seems it isn't.")

with (cwd / "preview-template.html").open("r", encoding="utf-8") as fd:
	template=fd.read()

def get_questions(fn: pathlib.Path):
	questions = [] # elements are ["question", [correct answers (1-indexed)], [answers]]
	state = 0
	with fn.open("r") as fd:
		for line in fd:
			if state == 0: # skip until start of question
				if line.strip().startswith("\\begin{Question}"):
					state = 1
					questions.append([[], [], []])
			elif state == 1: # read question
				if line.strip().startswith("\\end{Question}"):
					state = 2
					questions[-1][0] = "".join(questions[-1][0])
				else:
					questions[-1][0].append(line)
			elif state == 2: # skip until start of answer or new question
				if line.strip().startswith("\\begin{Answer"):
					state = 3
					questions[-1][2].append([])
					if "True" in line:
						questions[-1][1].append(len(questions[-1][2]))
					state = 3
				elif line.strip().startswith("\\begin{Question}"):
					state = 1
					questions.append([[], [], []])
			elif state == 3: # read answer
				if line.strip().startswith("\\end{Answer"):
					state = 2
					questions[-1][2][-1] = "".join(questions[-1][2][-1])
				else:
					questions[-1][2][-1].append(line)
	return questions

def get_preamble(fn: pathlib.Path):
	acc = []
	with fn.open("r") as fd:
		for line in fd:
			if line.startswith("\\begin{document}"):
				break
			elif line.startswith("\\documentclass"):
				acc.append("\\documentclass{standalone}\n")
			else:
				acc.append(line)
	return "".join(acc)

def sha1(x: str):
	return hashlib.sha1(x.encode(sys.getfilesystemencoding())).hexdigest()

def raw2image(code: str, preamble: str, outfile: pathlib.Path):
	tmpdir = pathlib.Path(tempfile.mkdtemp()).absolute()
	olddir = pathlib.Path().absolute()
	os.chdir(tmpdir)
	with (tmpdir / "fig.tex").open("w") as f:
		f.write(preamble)
		f.write("\n\\begin{document}\n")
		f.write(code)
		f.write("\n\\end{document}\n")
	p = subprocess.Popen(["pdflatex", "fig.tex"], stdout=subprocess.PIPE)
	p.communicate()
	p.terminate()
	os.chdir(olddir)
	subprocess.call(["convert", "-density", "144", str(tmpdir / "fig.pdf"), str(outfile)])
	shutil.rmtree(tmpdir)

def processblocks(blocks: list, preamble: str) -> None:
	# input: part of pandoc AST as list of dicts
	# will be modified (in-place): tikz and included pdfs will be turned into pngs
	if not isinstance(blocks, list):
		# probably reached literal string
		return
	for i, block in enumerate(blocks):
		if not isinstance(block, dict):
			return
		if block["t"] == "RawBlock":
			blocks[i] = handle_rawblock(block, preamble)
		elif block["t"] == "Image":
			blocks[i] = handle_image(block)
		else:
			if isinstance(block.get("c", None), list):
				processblocks(block["c"], preamble)

def handle_rawblock(block: dict, preamble: str) -> dict:
	assert block["t"] == "RawBlock"
	kind, content = block["c"]
	if kind == "latex":
		outfile = (imagedir / sha1(content)).with_suffix(".png")
		if not outfile.is_file():
			print(f"Make a picture out of this {kind}", file=sys.stderr)
			print(f"{content[:80]}")
			raw2image(content, preamble, outfile)
		return {"t": "Para", "c": [{"t": "Image", "c": [["", [], []],
													[{"t": "Str", "c": "image"}],
													[str(outfile), ""]]}]}
	else:
		raise Exception(f"Error: no support for raw block type {kind}")

def handle_image(block: dict) -> dict:
	assert block["t"] == "Image"
	content = block["c"]
	filename = content[2][0]
	if filename.endswith(".png"):
		return block
	infile = filename
	outfile = (imagedir / sha1(filename)).with_suffix(".png")
	subprocess.call(["convert", "-density", "144", str(infile), str(outfile.with_suffix(".png"))])
	return {"t": "Image", "c": [["", [], []], [{"t": "Str", "c": "image"}], [str(outfile), ""]]}

def latex2ast(code: str):
	p = subprocess.Popen(["pandoc", "-f", "latex+raw_tex", "-t", "json"],
						 stdin=subprocess.PIPE, stdout=subprocess.PIPE)
	out, err = p.communicate(code.encode())
	p.terminate()
	tree = json.loads(out.decode())
	return tree

def ast2html(tree: dict):
	p = subprocess.Popen(["pandoc", "-f", "json", "-t", "html", "--mathjax"],
						 stdin=subprocess.PIPE, stdout=subprocess.PIPE)
	out, err = p.communicate(json.dumps(tree).encode())
	p.terminate()
	return out.decode()

def latex2html(code: str, preamble: str):
	tree = latex2ast(code)
	blocks = tree["blocks"]
	processblocks(blocks, preamble)
	return ast2html(tree)

# for generating preview / checking document
questions = get_questions(texfile)
preamble = get_preamble(texfile)
generatePreview = True
if generatePreview:
	preview = []
os.chdir(workdir)
with BlackboardQuiz.Package(packagename) as package:
	with package.createPool(poolname) as pool:
		for question, correct, answers in questions:
			questionhtml = latex2html(question, preamble)
			answershtml = [latex2html(a, preamble) for a in answers]
			pool.addQuestion(text=questionhtml, answers=answershtml, correct=correct)
			if generatePreview:
				preview.append(questionhtml)
				for index, answer in enumerate(answershtml, 1):
					preview.append(f"Answer {index}{' (correct)' if index in correct else ''}")
					preview.append(answer)
				preview.append("<hr/>")

os.chdir(cwd)
if generatePreview:
	with (workdir / (packagename + "-preview.html")).open("w", encoding="utf-8") as fd:
		fd.write(template.format("\n".join(preview)))
